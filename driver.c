/*
* This program is designed to encrypt and decrypt a standard text file.
*/

#include <stdio.h>

void getKey();
void createFile();
void encrypt();
void decrypt();

// void main () {
void main (int argc, char *argv[]) {
	printf("Running...\n");
	
	// GETTING THE KEY
	int key = 1;
	printf("Enter the key:\n");
	key = atoi(argv[1]);

	printf("Key is: %d\n", key);

	// RETRIEVING TEXT
	int idx = 2;
	printf("Enter the text to encrypt...\n");

	FILE *fp;
	char str[100];
    fp=fopen("/tmp/test.txt","w+");
    
    while (idx < argc) {
    	// fputs(argv[2],fp);      	
    	fputs(argv[idx],fp);
    	fputs(" ", fp);
    	idx++;
    } 
    
    fclose(fp);
    printf("File created successfully.\n");

	getKey();
	createFile();
	encrypt();
}

void getKey() {
		
}

/**
* Creates a textfile based on cmd line input
*/
void createFile() {
	
}

/*
* Encrypts the given text file.
*/
void encrypt() {
	
}

/**
* Decrypts a text file.
*/ 
void decrypt() {

}